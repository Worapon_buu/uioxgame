/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author max15
 */
public class Board {

	private char [][] Table  = new char[][] {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
	private Player x;
	private Player o ;
	private Player winner;
	private Player currentPlayer;
	private int turnCount;
	
	public Board(Player x, Player o) {
		this.x =x;
		this.o =o;
		currentPlayer =x;
		winner=null;
		turnCount =0;
	}
	
	public char[] [] getTable(){
		return Table ;
	}
	
	public Player getCurrentPlayer(){
		return currentPlayer;
	}

	public boolean setTable(int row, int col) {
		if(Table[row][col]=='-') {
			Table[row][col] = currentPlayer.getName();
			return true;
		}
		return false;
	}
	
	private boolean checkRow(int row) {
		for(int col=0; col<Table[row].length;col++) {
			if(Table[row][col]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkRow(){
		if(checkRow(0) || checkRow(1) || checkRow(2)) {
			return true;
		}
		return false;
	}
	
	
	private boolean checkCol(int col) {
		for(int row=0; row<Table[col].length;row++) {
			if(Table[row][col]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkCol(){
		if(checkCol(0) || checkCol(1) || checkCol(2)) {
			return true;
		}
		return false;
	}
	private boolean checkDiagonal1() {
		for(int i=0;i<Table.length;i++) {
			if(Table[i][i]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkDiagonal2() {
		for(int i=0;i<Table.length;i++) {
			if(Table[2-i][i]!=currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkDraw() {
		if(turnCount==8) {
			x.draw();
			o.draw();
			return true;
		}
		return false;
	}
	
	private boolean checkWin() {
		if(checkRow() || checkCol() || checkDiagonal1() ||checkDiagonal2()) {
			winner=currentPlayer;
			if(currentPlayer==x) {
				x.win();
				o.lose();
			}else {
				o.win();
				x.lose();
			}
			return true;
		}
		return false ;
	}
	
	public Player getWinner() {
		return winner;
	}
	public boolean isFinish() {
		if(checkWin()) {
			return true;
		}
		if(checkDraw()) {
			return true;
		}
			
		return false;
	}
	
	public void switchPlayer() {
		if(currentPlayer==x) {
			currentPlayer=o;
		}else {
			currentPlayer=x;
		}
		turnCount++;
	}
	
}

